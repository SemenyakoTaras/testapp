﻿using System;
using CompanyManager.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace CompanyManager.Tests
{
    public class BaseTest : IDisposable
    {
        protected BlogContext _context;
        public BaseTest()
        {
            var optionsBuilder = new DbContextOptionsBuilder<BlogContext>();

            optionsBuilder.UseSqlServer("Server=SR-HOME;Database=blogDbTest;Trusted_Connection=True;MultipleActiveResultSets=true");

            _context = new BlogContext(optionsBuilder.Options);

            _context.Database.Migrate();

            _context.Database.EnsureCreated();
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
        }
    }
}
