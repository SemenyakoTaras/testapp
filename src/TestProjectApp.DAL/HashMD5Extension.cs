﻿using System.Security.Cryptography;
using System.Text;

namespace CompanyManager.DAL
{
    public static class HashMD5Extension
    {
        public static string Hash(string input)
        {
            var result = MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(input));
            return Encoding.ASCII.GetString(result);
        }
    }
}
