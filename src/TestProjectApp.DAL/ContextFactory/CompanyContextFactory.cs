﻿using CompanyManager.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace CompanyManager.DAL.ContextFactory
{
    public class CompanyContextFactory: IDbContextFactory<BlogContext>
    {
        public BlogContext Create(DbContextFactoryOptions options)
        {
            var builder = new DbContextOptionsBuilder<BlogContext>();
            builder.UseSqlServer("Server=SR-HOME;Database=blogDb;Trusted_Connection=True;MultipleActiveResultSets=true");
            return new BlogContext(builder.Options);
        }
    }
}
