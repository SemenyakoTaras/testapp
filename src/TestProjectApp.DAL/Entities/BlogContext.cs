﻿using Microsoft.EntityFrameworkCore;

namespace CompanyManager.DAL.Entities
{
    public class BlogContext : DbContext
    {
        public BlogContext(DbContextOptions<BlogContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Tag> Tags { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasAlternateKey(c => c.Email);
            modelBuilder.Entity<Category>().HasAlternateKey(c => c.Name);

            modelBuilder.Entity<Article>().HasAlternateKey(c => c.Title);
            modelBuilder.Entity<Article>().HasAlternateKey(c => c.URL);
            modelBuilder.Entity<Article>().Property(c => c.InDraft).HasDefaultValue(true);

            modelBuilder.Entity<Tag>().HasAlternateKey(c => c.Name);
            modelBuilder.Entity<Role>().HasAlternateKey(c => c.Name);

            modelBuilder.Entity<Image>().HasAlternateKey(c => c.Filename);

            modelBuilder.Entity<ArticleTag>()
            .HasKey(t => new { t.ArticleId, t.TagId });

            modelBuilder.Entity<ArticleTag>()
                .HasOne(sc => sc.Article)
                .WithMany(s => s.ArticleTags)
                .HasForeignKey(sc => sc.ArticleId);

            modelBuilder.Entity<ArticleTag>()
                .HasOne(sc => sc.Tag)
                .WithMany(c => c.ArticleTags)
                .HasForeignKey(sc => sc.TagId);
        }
    }
}
