﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CompanyManager.DAL.Entities
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]        
        public string PasswordHash { get; set; }
        [Required]    
        [MinLength(2)]
        [MaxLength(50)]   
        public string Firstname { get; set; }
        [Required]
        [MinLength(2)]
        [MaxLength(50)]
        public string Lastname { get; set; }

        public int RoleId { get; set; }
        public virtual Role Role { get; set; }              

        public virtual ICollection<Article> Articles { get; set; }
    }
}
