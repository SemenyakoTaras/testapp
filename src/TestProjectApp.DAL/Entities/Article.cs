﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CompanyManager.DAL.Entities
{
    public class Article
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(150)]
        public string Title { get; set; }

        public string Description { get; set; }

        [Required]
        [MinLength(3)]
        public string Content { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string URL { get; set; }
        public DateTime? DatePublished { get; set; }
        public bool InDraft { get; set; }

        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public int UserId { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<Image> Images { get; set; }
        public virtual ICollection<ArticleTag> ArticleTags { get; set; }
    }
}
