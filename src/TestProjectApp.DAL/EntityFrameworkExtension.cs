﻿using CompanyManager.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace CompanyManager.DAL
{
    public static class EntityFrameworkExtension
    {
        public static void AddEntityFramework(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<BlogContext>(options =>
                    options.UseSqlServer(connectionString));
        }
    }
}
