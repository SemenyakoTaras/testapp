﻿using System.Collections.Generic;
using System.Linq;
using CompanyManager.DAL.Entities;

namespace CompanyManager.DAL.Seeds
{
    public static class DbInitializer
    {
        public static void Initialize(BlogContext context)
        {
            context.Database.EnsureCreated();

            if (context.Users.Any()) return;

            var roles = new List<Role>() { new Role() { Name = "Admin" }, new Role() { Name = "Moderator" }, new Role() { Name = "Regular" } };

            foreach (var role in roles)
                context.Roles.Add(role);

            var user = new User()
            {
                Email = "test@test.com",
                PasswordHash = HashMD5Extension.Hash("123123"),
                RoleId = roles[0].Id,
                Firstname = "Taras",
                Lastname = "Semenyako"
            };

            context.Users.Add(user);
            context.SaveChanges();

        }
    }
}
