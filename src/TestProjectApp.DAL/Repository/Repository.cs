﻿using System;
using System.Linq;
using System.Linq.Expressions;
using CompanyManager.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace CompanyManager.DAL.Repository
{
    public class Repository : IRepository
    {
        protected readonly BlogContext Context;

        public Repository(BlogContext context)
        {
            Context = context;
        }

        public virtual IQueryable<TEntity> Query<TEntity>() where TEntity : class
        {
            var query = Context.Set<TEntity>().AsQueryable();
            return query;
        }

        public virtual TEntity Get<TEntity>(Expression<Func<TEntity, bool>> predicate = null) where TEntity : class
        {
            var result = GetList(predicate).FirstOrDefault();
            return result;
        }

        public virtual TEntity Get<TEntity>(Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> entityInclude) where TEntity : class
        {
            var result = GetList(predicate, entityInclude).FirstOrDefault();
            return result;
        }

        public IQueryable<TEntity> GetList<TEntity>(Expression<Func<TEntity, bool>> predicate = null) where TEntity : class
        {
            return predicate == null ? Query<TEntity>().AsNoTracking() : Query<TEntity>().Where(predicate).AsNoTracking();
        }

        public IQueryable<TEntity> GetList<TEntity>(Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> entityInclude) where TEntity : class
        {
            var result = GetList(predicate);

            return entityInclude == null
                ? result
                : entityInclude(result);
        }

        public bool Any<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            return predicate != null ? Query<TEntity>().Any(predicate) : Query<TEntity>().Any();
        }

        public void Add<TEntity>(TEntity entity) where TEntity : class
        {
            Context.Set<TEntity>().Add(entity);
        }

        public void Update<TEntity>(TEntity model) where TEntity : class
        {
            Context.Entry(model).State = EntityState.Modified;
        }

        public void Remove<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            var entity = Get(predicate);
            Remove(entity);
        }

        public void Remove<TEntity>(TEntity entity) where TEntity : class
        {
            Context.Set<TEntity>().Remove(entity);
        }

        public void Commit()
        {
            Context.SaveChanges();
        }
    }
}
