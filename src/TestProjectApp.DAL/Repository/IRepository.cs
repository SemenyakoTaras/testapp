﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace CompanyManager.DAL.Repository
{
    public interface IRepository
    {
        IQueryable<TEntity> Query<TEntity>()
            where TEntity : class;
        
        TEntity Get<TEntity>(Expression<Func<TEntity, bool>> predicate = null)
            where TEntity : class;

        TEntity Get<TEntity>(Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> entityInclude)
            where TEntity : class;

        IQueryable<TEntity> GetList<TEntity>(Expression<Func<TEntity, bool>> predicate = null)
            where TEntity : class;

        IQueryable<TEntity> GetList<TEntity>(Expression<Func<TEntity, bool>> predicate,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> entityInclude)
            where TEntity : class;

        bool Any<TEntity>(Expression<Func<TEntity, bool>> predicate = null)
            where TEntity : class;

        void Add<TEntity>(TEntity entity)
            where TEntity : class;

        void Update<TEntity>(TEntity model)
            where TEntity : class;

        void Remove<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class;

        void Remove<TEntity>(TEntity entity)
            where TEntity : class;

        void Commit();
    }
}
