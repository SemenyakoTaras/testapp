﻿import { Injectable } from "@angular/core";
import { ConnectionBackend, RequestOptions, Request, RequestOptionsArgs, Response, Http, Headers } from "@angular/http";
import { LocalStorageService } from 'angular-2-local-storage';
import { Observable } from "rxjs/Rx";
import { URLConstants } from '../constants/url.constants';
import { Storage } from '../enums/storage.enum';

@Injectable()
export class HttpInterceptor extends Http {   

    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions, private localStorage: LocalStorageService) {
        super(backend, defaultOptions);       
    }

    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        return super.request(url, options);
    }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {           
        return super.get(url, this.getRequestOptionArgs(url,options));
    }

    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {              
        return super.post(url, body, this.getRequestOptionArgs(url,options));
    }

    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {      
        return super.put(url, body, this.getRequestOptionArgs(url,options));
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {       
        return super.delete(url, this.getRequestOptionArgs(url,options));
    }

    

    private getRequestOptionArgs(url: string, options?: RequestOptionsArgs): RequestOptionsArgs {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
        }
        
        options.headers.append('Content-Type', 'application/json');
        options.headers.append('Access-Control-Allow-Origin', '*');
        
        if (URLConstants.urlWithoutAuth.some(x => x == url)) return options;

        let currentUser = JSON.parse(this.localStorage.get<string>(Storage[Storage.CurrentUser]));

        if (currentUser !== {} && currentUser!=null) {
            options.headers.append('Authorization', 'Bearer ' + currentUser.accessToken);
        }       

        return options;
    }
}