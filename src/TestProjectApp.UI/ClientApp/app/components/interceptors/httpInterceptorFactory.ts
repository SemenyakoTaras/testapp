﻿import { XHRBackend, Http, RequestOptions } from "@angular/http";
import { HttpInterceptor } from "../interceptors/httpInterceptor";
import { LocalStorageService } from 'angular-2-local-storage';

export function httpFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions, localstorage:LocalStorageService): Http {
    return new HttpInterceptor(xhrBackend, requestOptions, localstorage);
}