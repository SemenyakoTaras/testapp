﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { MessageService } from '../services/message.service';
import { Role } from '../enums/role.enum';
import { Storage } from '../enums/storage.enum';
import { AuthenticationService } from '../services/authentication.service';

import { CustomLocalStorageService } from '../services/customLocalStorage.service';

import { CategoryService } from '../admin/category/category.service';
import { CategoryModel } from '../admin/category/category.model';

@Component({
    selector: 'nav-menu-top',
    template: require('./navmenutop.component.html'),
    styles: [require('./navmenutop.component.css')]
})
export class NavMenuTopComponent {   
    public IsAdministrator = false;
    public categoryList: CategoryModel[];

    constructor(private router: Router,
        private message: MessageService,
        private authenticationService: AuthenticationService,
        private localStrorage: CustomLocalStorageService,
        private categoryService: CategoryService) {         
        
        if (this.localStrorage.storageIsExist(Storage[Storage.CurrentUser])) {
            var currentUser = this.localStrorage.getStorage(Storage[Storage.CurrentUser]);

            if (currentUser.role == Role[Role.Admin])
                this.IsAdministrator = true;
        }

        //update nav bar after login 
        this.message.getMessage().subscribe(result => {            
            if (result == Role[Role.Admin])
                this.IsAdministrator = true;
        });   

        categoryService.getAll().subscribe(result => {
            this.categoryList = result;
        });     
    }

    logout(event) {
        event.preventDefault();        
        this.authenticationService.logout();
        this.IsAdministrator = false;
        this.message.clearMessage();
        this.router.navigate(['/articles']);

    }
}