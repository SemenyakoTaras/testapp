﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../services/authentication.service';
import { MessageService } from '../services/message.service'
import { Role } from '../enums/role.enum'

@Component({
    selector: 'login',
    template: require('./login.component.html')
})
export class LoginComponent {
    model: any = {};
    error = '';

    constructor(private router: Router, private authenticationService: AuthenticationService, private message: MessageService) { }

    login() {
        this.authenticationService.login(this.model.email, this.model.password)
            .subscribe(result => {
                
                this.message.sendMessage(Role[Role.Admin]);
                this.router.navigate(['/counter']);
            },
            error => {
               
                this.error = error.Error.Message;
            });
    }
}
