﻿
export class URLConstants {
    public static  baseUrl: string = "http://localhost:4431/api/";
    public static version1: string = "v1/";

    public static getBaseUrl(): string {
        return this.baseUrl + this.version1;
    }

    public static getBaseUrlVersion(version: string): string {
        return this.baseUrl + version;
    }

    public static accountLogin = "account/login";

    public static categoryGetAll = "category/get"; 
    public static categoryAdd = "category/create";   
    public static categoryGetById = "category/get/";
    public static categoryRemove = "category/delete/";   

    public static articleGetAll = "article/get";
    public static articleGetByUrl = "article/get";
    public static articleAdd = "article/create";

    //temporary
    public static articleGetAllForAdmin = "article/get-admin";
    public static articleGetByUrlForAdmin = "article/get-admin";


    public static urlWithoutAuth: string[] = [
        URLConstants.getBaseUrl() + URLConstants.accountLogin,
        URLConstants.getBaseUrl() + URLConstants.categoryGetAll,

        URLConstants.getBaseUrl() + URLConstants.articleGetAll,
        URLConstants.getBaseUrl() + URLConstants.articleGetByUrl
    ];

}