﻿import { Component } from '@angular/core';

@Component({
    selector: 'nav-menu-right-bar',
    template: require('./navmenurightbar.component.html')
})
export class NavMenuRightBarComponent {
}