﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';

@Component({
    selector: 'pagination',    
    template: require('./pager.template.html')
})
export class Pagination {    
    @Input() current: number;
    @Input() total: number;
    @Input() url: string;
}