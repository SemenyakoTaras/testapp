﻿import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import { URLConstants } from '../../../components/constants/url.constants';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { ArticleListModel } from './articleList.model';
import { ArticleModel } from './article.model';


@Injectable()
export class ArticleService {

    constructor(private http: Http) { }

    getAll(page?: number, isAdmin: boolean = false): Observable<ArticleListModel> {
        var part = isAdmin ? URLConstants.articleGetAllForAdmin : URLConstants.articleGetAll;
        var url = page ? URLConstants.getBaseUrl() + part + "/" + page.toString() : URLConstants.getBaseUrl() + part;
        return this.http.get(url)
            .map((response: Response) => {
                return response.json().data;
            })
            .catch((error: Response) => {
                return Observable.throw(error.json());
            });
    }

    get(url: string, isAdmin: boolean = false): Observable<ArticleModel> {
        var part = isAdmin ? URLConstants.articleGetByUrlForAdmin : URLConstants.articleGetByUrl;
        var u = URLConstants.getBaseUrl() + part + "/" + url;
        return this.http.get(u)
            .map((response: Response) => {
                return response.json().data;
            })
            .catch((error: Response) => {
                return Observable.throw(error.json());
            });
    }



    //add(category: CategoryModel) {

    //    return this.http.post(URLConstants.getBaseUrl() + URLConstants.categoryAdd, category)
    //        .map((response: Response) => {
    //            return response.json().data;
    //        })
    //        .catch((error: Response) => {
    //            return Observable.throw(error.json());
    //        });
    //}

    //delete(categoryId: number) {
    //    return this.http.delete(URLConstants.getBaseUrl() + URLConstants.categoryRemove + categoryId)
    //        .map((response: Response) => {
    //            return response.json().data;
    //        })
    //        .catch((error: Response) => {
    //            return Observable.throw(error.json());
    //        });
    //}

}