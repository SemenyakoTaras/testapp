﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { ArticleService } from './article.service';
import { ArticleModel } from './article.model';

import { Role } from '../../enums/role.enum';
import { Storage } from '../../enums/storage.enum';
import { CustomLocalStorageService } from "../../services/customLocalStorage.service";

@Component({
    selector: 'article-view',
    template: require('./article-view.component.html'),
    styles: [require('./articles.component.css')]
})

export class ArticleViewComponent implements OnInit {
    article: ArticleModel;
    previousUrl: any;
    isAdministrator = false;

    constructor(private router: Router, private activatedRoute: ActivatedRoute, private articleService: ArticleService, private location: Location, private localStrorage: CustomLocalStorageService) { }

    ngOnInit() {
        this.article = new ArticleModel();

        if (this.localStrorage.storageIsExist(Storage[Storage.CurrentUser])) {
            var currentUser = this.localStrorage.getStorage(Storage[Storage.CurrentUser]);

            if (currentUser.role == Role[Role.Admin])
                this.isAdministrator = true;
        }
        
        this.activatedRoute.params.subscribe(params => {
            var url = params["url"];
            this.getArticle(url);
        });
    }

     getArticle(url?: string) {
        if (url) {
            this.articleService.get(url).subscribe(ar => {
                this.article = ar;
            });
        } else {
            this.router.navigate(['/articles']);
        }
    }

    back() {
        this.location.back();
    }
}
