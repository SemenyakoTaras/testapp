﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ArticleModel } from './article.model';
import { ArticleService } from './article.service';

@Component({
    selector: 'article-add-edit',
    template: require('./article-add-edit.component.html')    
})

export class ArticleAddEditComponent implements OnInit {
    public article: ArticleModel;
    public IsEdit: boolean;
    constructor(private activatedRoute: ActivatedRoute, private articleService: ArticleService) { }

    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            debugger;
            var url = params["url"];
            if (url) {
                this.articleService.get(url).subscribe(article => {
                    this.article = article;
                });
            } else {
                this.article = new ArticleModel();
            }
        });       
    }

    
}