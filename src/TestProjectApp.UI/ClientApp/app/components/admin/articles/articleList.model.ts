﻿import { ArticlePresentModel } from './articlepresent.model';
import { PagerModel } from '../pager.model';

export class ArticleListModel {
    articles: ArticlePresentModel[];
    page: PagerModel;
}