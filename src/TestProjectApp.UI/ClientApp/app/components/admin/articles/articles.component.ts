﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ArticleListModel } from './articleList.model';
import { ArticleService } from './article.service';
import { ArticlePresentModel } from './articlepresent.model';
import { PagerModel } from '../pager.model';

import { Role } from '../../enums/role.enum';
import { Storage } from '../../enums/storage.enum';
import { CustomLocalStorageService } from "../../services/customLocalStorage.service";


@Component({
    selector: 'articles',
    template: require('./articles.component.html'),
    styles: [require('./articles.component.css')]
})

export class ArticlesComponent implements OnInit {
    articleList: ArticlePresentModel[];    
    page: PagerModel;
    isAdministrator = false;    

    constructor(private router: Router, private activatedRoute: ActivatedRoute, private articleService: ArticleService, private localStrorage: CustomLocalStorageService) { }

    ngOnInit() {
        if (this.localStrorage.storageIsExist(Storage[Storage.CurrentUser])) {
            var currentUser = this.localStrorage.getStorage(Storage[Storage.CurrentUser]);

            if (currentUser.role == Role[Role.Admin])
                this.isAdministrator = true;
        }
        
        this.activatedRoute.params.subscribe(params => {
            var pageId = params["page"];
            if (pageId) {
                this.getArticles(pageId);
            } else {
                this.getArticles();
            }
        });
    }

    public getArticles(page?: number) {
        this.articleService.getAll(page, this.isAdministrator).subscribe(result => {            
            this.articleList = result.articles;
            this.page = result.page;       
        });
    }   
}
