﻿import { CategoryModel } from '../category/category.model';
import { UserModel } from '../../login/usermodel';

export class ArticlePresentModel {
    URL: string;
    Title: string;
    Description: string;
    DatePublished: Date;
    User: UserModel;
    Category: CategoryModel;
}