﻿import { CategoryModel } from '../category/category.model';
import { UserModel } from '../../login/usermodel';

export class ArticleModel {
     Id:number;
     Title: string;
     Description: string;
     Content: string;
     URL: string;
     InDraft: boolean;
     User: UserModel;
     Category: CategoryModel;
     Categories: CategoryModel[];
}