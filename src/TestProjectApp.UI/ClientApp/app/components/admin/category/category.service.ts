﻿import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { URLConstants } from '../../../components/constants/url.constants';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { CategoryModel } from './category.model';

@Injectable()
export class CategoryService {    

    constructor(private http: Http) { }

    getAll(): Observable<CategoryModel[]> {
        return this.http.get(URLConstants.getBaseUrl() + URLConstants.categoryGetAll)
            .map((response: Response) => {               
                return response.json().data;
            })
            .catch((error: Response) => {                
                return Observable.throw(error.json());
            });
    }

    add(category: CategoryModel) {        
        
        return this.http.post(URLConstants.getBaseUrl() + URLConstants.categoryAdd, category)
            .map((response: Response) => {                
                return response.json().data;
            })
            .catch((error: Response) => {
                return Observable.throw(error.json());
            });
    }

    delete(categoryId: number) {
        return this.http.delete(URLConstants.getBaseUrl() + URLConstants.categoryRemove + categoryId)
            .map((response: Response) => {
                return response.json().data;
            })
            .catch((error: Response) => {
                return Observable.throw(error.json());
            });
    }
  
}