﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryModel } from './category.model';
import { CategoryService } from './category.service';

@Component({
    selector: 'category-add',
    template: require('./categoryAdd.component.html')
})

export class CategoryAddComponent {

    public category: CategoryModel;
    public error = '';
    constructor(private router: Router, private categoryService: CategoryService) {
        this.category = new CategoryModel();
    }

    addCategory() {        
        this.categoryService.add(this.category).subscribe(
            result => {                
                this.back();
            }, error => {                
                this.error = error.Error.Message;
            });
    }

    back() {
        this.router.navigate(['admin/category']);
    }
}
