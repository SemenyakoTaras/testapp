﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryModel } from './category.model';
import { CategoryService } from './category.service';

@Component({
    selector: 'category-list',
    template: require('./categoryList.component.html')
})

export class CategoryListComponent {

    public categories: CategoryModel[];
    public error = "";
    constructor(private router: Router, private categoryService: CategoryService) {

        categoryService.getAll().subscribe(result => {
            this.categories = result;
        });
    }

    removeCategory(categoryId: number) {        
        this.categoryService.delete(categoryId).subscribe(result => {           
            let index = this.categories.findIndex(c => c.id == categoryId);
            this.categories.splice(index, 1);

        }, error => {
            this.error = error.Error.Message;
        });
    }

    addCategory() {
        this.router.navigate(['/admin/category-add']);
    }
}
