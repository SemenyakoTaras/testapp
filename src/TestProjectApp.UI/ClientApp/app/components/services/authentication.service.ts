﻿import { Injectable } from '@angular/core';
import { Http, Response} from '@angular/http';
import { URLConstants } from '../constants/url.constants';
import { Observable } from 'rxjs';
import { CustomLocalStorageService } from '../services/customLocalStorage.service';
import { Storage } from '../enums/storage.enum';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
    public accessToken: string;    

    constructor(private http: Http, private localStorage: CustomLocalStorageService) { 

        var currentUser = this.localStorage.getStorage(Storage[Storage.CurrentUser]);
        this.accessToken = currentUser && currentUser.accessToken;
    }

    login(email: string, password: string): Observable<any> {
       
        return this.http.post(URLConstants.getBaseUrl() + URLConstants.accountLogin, JSON.stringify({ email: email, password: password }))
            .map((response: Response) => {
                
                let result = response.json();
                this.accessToken = result.data.accessToken;
                this.localStorage.setStorage(Storage[Storage.CurrentUser], { login: result.data.login, accessToken: result.data.accessToken, role: result.data.role });               
                return true;

            })
            .catch((error: Response) => {
                
                return Observable.throw(error.json());
            });
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.accessToken = null;
        this.localStorage.removeStorage(Storage[Storage.CurrentUser]);
    }
}