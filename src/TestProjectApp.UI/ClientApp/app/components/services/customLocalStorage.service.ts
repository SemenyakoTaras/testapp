﻿import { Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class CustomLocalStorageService {  

    public localStorage: LocalStorageService;
    constructor(private LocalStorageService: LocalStorageService) {              
        this.localStorage = LocalStorageService;        
    }

    storageIsExist(storageName: string): boolean {
        if (this.localStorage.get(storageName))
            return true;

        return false;
    }

    getStorage(storageName: string): any {
        return JSON.parse(this.localStorage.get<string>(storageName));
    }

    setStorage(storageName: string, data:any) {
        this.localStorage.set(storageName, JSON.stringify(data));
    }

    removeStorage(storageName: string) {
        this.localStorage.remove(storageName);
    }    
}