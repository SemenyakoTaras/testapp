﻿import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { CustomLocalStorageService } from '../services/customLocalStorage.service';
import { Storage } from '../enums/storage.enum';
import { Role } from '../enums/role.enum'

@Injectable()
export class AuthGuard implements CanActivate {
    
    constructor(private router: Router, private localStorage: CustomLocalStorageService) { }

    canActivate() {
        if (this.localStorage.storageIsExist(Storage[Storage.CurrentUser]))
        {
            var currentUser = this.localStorage.getStorage(Storage[Storage.CurrentUser]);

            if (currentUser.role == Role[Role.Admin])
                return true;
        }
        this.router.navigate(['/home']);
        return false;
    }
}