import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UniversalModule } from 'angular2-universal';
import { FormsModule } from '@angular/forms';
import { LocalStorageModule, LocalStorageService  } from 'angular-2-local-storage';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';

import "froala-editor/js/froala_editor.pkgd.min.js";
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

import { AppComponent } from './components/app/app.component';

//COMPONENTS
//MENU COMPONENTS
import { NavMenuTopComponent } from './components/navmenutop/navmenutop.component';
import { NavMenuRightBarComponent } from './components/navmenurightbar/navmenurightbar.component';


import { ArticlesComponent } from './components/admin/articles/articles.component';
import { ArticleViewComponent } from './components/admin/articles/article-view.component';
import { ArticleAddEditComponent } from './components/admin/articles/article-add-edit.component';
import { LoginComponent } from './components/login/login.component';

//CATEGORY COMPONENTS
import { CategoryListComponent } from './components/admin/category/categoryList.component';
import { CategoryAddComponent } from './components/admin/category/categoryAdd.component';


import { AuthGuard } from './components/guards/auth.guard';
import { AuthenticationService } from './components/services/authentication.service';
import { CustomLocalStorageService } from './components/services/customLocalStorage.service';
import { MessageService } from './components/services/message.service';
import { CategoryService } from './components/admin/category/category.service';
import { ArticleService } from './components/admin/articles/article.service'

//DIRECTIVES
import { Pagination } from './components/directives/pagination/pager.directive';

//INTERCEPTORS
import { httpFactory } from "./components/interceptors/httpInterceptorFactory";

@NgModule({
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent,

        //MENU
        NavMenuTopComponent,
        NavMenuRightBarComponent,

        //COMPONENTS        
        ArticlesComponent,
        ArticleViewComponent,
        ArticleAddEditComponent,
        LoginComponent,
        CategoryListComponent,
        CategoryAddComponent,

        //DIRECTIVES
        Pagination,
    ],
    imports: [
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
        UniversalModule, // Must be first import. This automatically imports BrowserModule, HttpModule, and JsonpModule too.
        RouterModule.forRoot([
            { path: '', redirectTo: 'articles', pathMatch: 'full' },
            { path: 'articles', component: ArticlesComponent },
            { path: 'articles/page/:page', component: ArticlesComponent },
            { path: 'article/:url', component: ArticleViewComponent },

            { path: 'admin/article-add-edit', component: ArticleAddEditComponent, canActivate: [AuthGuard] },
            { path: 'admin/article-add-edit/:url', component: ArticleAddEditComponent, canActivate: [AuthGuard] },
            { path: 'login', component: LoginComponent },

            { path: 'admin/category', component: CategoryListComponent, canActivate: [AuthGuard] },
            { path: 'admin/category-add', component: CategoryAddComponent, canActivate: [AuthGuard] },            
            { path: '**', redirectTo: 'articles' }
        ]),
        FormsModule,
        LocalStorageModule.withConfig({
            prefix: 'app',
            storageType: 'localStorage'
        })
    ],
    providers: [
        CustomLocalStorageService,
        AuthGuard,
        AuthenticationService,
        MessageService,
        CategoryService,
        {
            provide: Http,
            useFactory: httpFactory,
            deps: [XHRBackend, RequestOptions, LocalStorageService]
        },
        ArticleService
    ]
})

export class AppModule { }
