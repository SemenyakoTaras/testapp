﻿namespace CompanyManager.BLL.Models
{
    public class ResultModel
    {
        public ResultModel(object data)
        {
            Data = data;
            Error = null;
        }

        public ResultModel(object data, ErrorModel error = null)
        {
            if (error == null)
            {
                Data = data;
                Error = null;
            }

            else
            {
                Data = null;
                Error = error;
            }
        }

        public object Data { get; set; }
        public ErrorModel Error { get; set; }

    }
}
