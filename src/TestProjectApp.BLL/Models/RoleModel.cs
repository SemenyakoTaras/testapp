﻿using System.ComponentModel.DataAnnotations;

namespace CompanyManager.BLL.Models
{
    public class RoleModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "The role name is required")]
        public string Name { get; set; }
    }
}
