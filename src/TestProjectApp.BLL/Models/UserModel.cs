﻿using System.ComponentModel.DataAnnotations;

namespace CompanyManager.BLL.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The password is required")]
        [MinLength(6, ErrorMessage = "The password should have more then 5 symbols")]
        [MaxLength(20, ErrorMessage = "The password should have less then 20 symbols")]
        public string Password { get; set; }        

        [Required(ErrorMessage = "The role is required")]
        public int RoleId { get; set; }       

    }    
}
