﻿using System.ComponentModel.DataAnnotations;

namespace CompanyManager.BLL.Models
{
    public class TagModel
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Tag name is required")]
        [StringLength(50, ErrorMessage = "Tag name must contain more then 2 symbols and less then 50", MinimumLength = 2)]
        public string Name { get; set; }
    }
}
