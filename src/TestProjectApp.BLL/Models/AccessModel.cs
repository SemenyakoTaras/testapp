﻿namespace CompanyManager.BLL.Models
{
    public class AccessModel
    {
        public string Login { get; set; }
        public string AccessToken { get; set; }
        public string Role { get; set; }
    }
}
