﻿namespace CompanyManager.BLL.Models
{
    public class PagerModel
    {
        public int Current { get; set; }
        public double Total { get; set; }
    }
}
