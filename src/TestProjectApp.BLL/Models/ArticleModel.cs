﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CompanyManager.BLL.Models
{
    public class ArticleModel
    {
        public int Id { get; set; }
        [Required(AllowEmptyStrings =false,ErrorMessage ="The title is required")]
        [StringLength(150, ErrorMessage = "The title must contain more then 2 symbols and less then 150", MinimumLength = 3)]
        public string Title { get; set; }

        public string Description { get; set; }

        [Required(AllowEmptyStrings =false,ErrorMessage ="The content is required")]
        [MinLength(3, ErrorMessage = "The content field must contain at least 3 symbols")]
        public string Content { get; set; }

        [Required(AllowEmptyStrings =false,ErrorMessage ="The url is required")]
        [StringLength(50, ErrorMessage = "The url field must contain more then 3 symbols and less then 50", MinimumLength = 3)]
        public string URL { get; set; }
        public bool InDraft { get; set; }
        public CategoryModel Category { get; set; }
        public UserModel User { get; set; }

    }

    public class ArticlePresentModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
        public DateTime DatePublished { get; set; }
        public UserModel User { get; set; }
        public CategoryModel Category { get; set; }
    }

    public class ArticleListModel
    {
        public List<ArticlePresentModel> Articles { get; set; }
        public PagerModel Page { get; set; }
    }
}
