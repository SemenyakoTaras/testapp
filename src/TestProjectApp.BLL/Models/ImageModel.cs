﻿namespace CompanyManager.BLL.Models
{
    public class ImageModel
    {
        public string Filename { get; set; }
        public byte[] Bytes { get; set; }

        public string Path { get; set; }          
    }
}
