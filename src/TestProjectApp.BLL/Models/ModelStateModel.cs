﻿using System.Collections.Generic;

namespace CompanyManager.BLL.Models
{
    public class ModelStateModel
    {
        public string Property { get; set; }
        public List<string> Errors { get; set; } 
    }
}
