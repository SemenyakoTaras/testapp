﻿using System.Collections.Generic;

namespace CompanyManager.BLL.Models
{
    public class ErrorModel
    {
        public ErrorModel(string message)
        {
            Message = message;
        }
        public ErrorModel(string message, List<ModelStateModel> exceptionDetail)
        {
            Message = message;
            ExceptionDetail = exceptionDetail;
        }

        public string Message { get; set; }
        public List<ModelStateModel> ExceptionDetail { get; set; }
    }
}
