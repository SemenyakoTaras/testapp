﻿using System.ComponentModel.DataAnnotations;

namespace CompanyManager.BLL.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Category name is required")]
        [StringLength(50, ErrorMessage = "Category name must contain more then 2 symbols and less then 50", MinimumLength = 2)]
        public string Name { get; set; }
    }
}
