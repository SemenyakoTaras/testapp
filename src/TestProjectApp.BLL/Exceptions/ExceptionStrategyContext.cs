﻿using System;
using System.Collections.Generic;
using System.Net;
using CompanyManager.BLL.Exceptions.Interfaces;
using CompanyManager.BLL.Exceptions.Models;
using CompanyManager.BLL.Models;
using CompanyManager.BLL.Resources;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CompanyManager.BLL.Exceptions
{
    public class ExceptionStrategyContext
    {
        private static readonly Dictionary<string, Exception> _strategies = new Dictionary<string, Exception>();

        static ExceptionStrategyContext()
        {
            _strategies.Add(nameof(ModelValidationException), new ModelValidationException());
            _strategies.Add(nameof(NotFoundException), new NotFoundException());
            _strategies.Add(nameof(ExistException), new ExistException());
            _strategies.Add(nameof(UnauthorizedAccessException), new UnauthorizedAccessException());
            _strategies.Add(nameof(NotImplementedException), new NotImplementedException());
        }

        public static ExceptionModel Execute(string strategy, ExceptionContext context)
        {
            var concreteStrategy = _strategies[strategy] as IExceptionResponse;

            if (concreteStrategy != null) return concreteStrategy.BuildExceptionResponse(context);

            return new ExceptionModel { Error = new ErrorModel(ExceptionResource.ServerError), StatusCode = HttpStatusCode.InternalServerError };
        }
    }
}
