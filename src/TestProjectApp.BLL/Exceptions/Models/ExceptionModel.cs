﻿using System.Net;
using CompanyManager.BLL.Models;

namespace CompanyManager.BLL.Exceptions.Models
{
    public class ExceptionModel
    {
        public ErrorModel Error { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
