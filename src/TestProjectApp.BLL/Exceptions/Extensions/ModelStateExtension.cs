﻿using System.Collections.Generic;
using System.Linq;
using CompanyManager.BLL.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace CompanyManager.BLL.Exceptions.Extensions
{
    public static class ModelStateExtension
    {
        public static List<ModelStateModel> GetErrorList(this ModelStateDictionary modelState)
        {
            var errorList = new List<ModelStateModel>();
            foreach (var modelStateKey in modelState.Keys)
            {
                var modelStateVal = modelState[modelStateKey];
                var stateModel = new ModelStateModel()
                {
                    Property = modelStateKey,
                    Errors = modelStateVal.Errors.Select(c => c.ErrorMessage).ToList()
                };
                errorList.Add(stateModel);

            }
            return errorList;
        }
    }
}
