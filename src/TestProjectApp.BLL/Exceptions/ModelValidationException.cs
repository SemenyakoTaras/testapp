﻿using System;
using System.Net;
using CompanyManager.BLL.Exceptions.Extensions;
using CompanyManager.BLL.Exceptions.Interfaces;
using CompanyManager.BLL.Exceptions.Models;
using CompanyManager.BLL.Models;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CompanyManager.BLL.Exceptions
{
    public class ModelValidationException : Exception, IExceptionResponse
    {
        public ModelValidationException() { }
        public ModelValidationException(string message) : base(message) { }

        public ExceptionModel BuildExceptionResponse(ExceptionContext context)
        {
            var modelStateErrors = context.ModelState.GetErrorList();
            return new ExceptionModel()
            {
                Error = new ErrorModel(context.Exception.Message, modelStateErrors),
                StatusCode = HttpStatusCode.BadRequest
            };
        }
    }
}
