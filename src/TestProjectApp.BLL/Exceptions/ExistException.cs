﻿using System;
using System.Net;
using CompanyManager.BLL.Exceptions.Interfaces;
using CompanyManager.BLL.Exceptions.Models;
using CompanyManager.BLL.Models;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CompanyManager.BLL.Exceptions
{
    public class ExistException : Exception,IExceptionResponse
    {
        public ExistException() { }
        public ExistException(string message) : base(message) { }
        public ExceptionModel BuildExceptionResponse(ExceptionContext context)
        {
            return new ExceptionModel()
            {
                Error = new ErrorModel(context.Exception.Message),
                StatusCode = HttpStatusCode.BadRequest
            };
        }
    }
}
