﻿using CompanyManager.BLL.Exceptions.Models;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CompanyManager.BLL.Exceptions.Interfaces
{
    public interface IExceptionResponse
    {
        ExceptionModel BuildExceptionResponse(ExceptionContext context);
    }
}
