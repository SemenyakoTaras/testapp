﻿using System.Collections.Generic;
using CompanyManager.BLL.Models;
using CompanyManager.BLL.Services.Interfaces;
using CompanyManager.DAL.Entities;
using CompanyManager.DAL.Repository;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using CompanyManager.BLL.Exceptions;
using CompanyManager.BLL.Resources;
using AutoMapper;

namespace CompanyManager.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository _repository;
        private readonly IMapper _mapper;
        public UserService(IRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public List<UserModel> GetAll()
        {
            var users = _repository.GetList<User>(null, c => c.Include(z => z.Role)).ToList();
            var result = _mapper.Map<List<UserModel>>(users);
            return result;
        }

        public UserModel GetUserById(int userId)
        {
            if (_repository.Any<User>(x => x.Id == userId))
            {
                var user = _repository.Get<User>(c => c.Id == userId, c => c.Include(b => b.Role));                       
                var result = _mapper.Map<UserModel>(user);
                return result;
            }

            throw new NotFoundException(ExceptionResource.UserNotFound);
        }

        public void CreateUser(UserModel model)
        {
            var user = _mapper.Map<User>(model);
            _repository.Add(user);
            _repository.Commit();
        }

        public void UpdateUser(UserModel model)
        {
            var dbUser = _repository.Get<User>(c => c.Email == model.Email);
            _mapper.Map(model, dbUser);

            _repository.Update(dbUser);
            _repository.Commit();
        }       
    }
}
