﻿using CompanyManager.BLL.Services.Interfaces;
using System.Collections.Generic;
using CompanyManager.BLL.Models;
using CompanyManager.DAL.Repository;
using CompanyManager.DAL.Entities;
using CompanyManager.BLL.Resources;
using CompanyManager.BLL.Exceptions;
using AutoMapper;
using System.Linq;

namespace CompanyManager.BLL.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IRepository _repository;
        private readonly IMapper _mapper;
        public CategoryService(IRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public void CreateCategory(CategoryModel model)
        {
            if (_repository.Any<Category>(c => c.Name == model.Name))
                throw new ExistException(ExceptionResource.CategoryExist);

            var category = _mapper.Map<Category>(model);
            _repository.Add(category);
            _repository.Commit();
        }

        public CategoryModel Get(int Id)
        {
            if (_repository.Any<Category>(c => c.Id == Id))
            {
                var category = _repository.Get<Category>(c => c.Id == Id);
                var result = _mapper.Map<CategoryModel>(category);
                return result;
            }

            throw new NotFoundException(ExceptionResource.CategoryNotFound);
        }

        public List<CategoryModel> GetAll()
        {
            var categories = _repository.GetList<Category>().OrderByDescending(z => z.Name);
            var result = _mapper.Map<List<CategoryModel>>(categories);
            return result;
        }

        public void RemoveCategory(int Id)
        {
            if (!_repository.Any<Category>(c => c.Id == Id))            
                throw new NotFoundException(ExceptionResource.CategoryNotFound);                
            
            _repository.Remove<Category>(c => c.Id == Id);
            _repository.Commit();
        }
    }
}
