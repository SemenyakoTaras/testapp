﻿using System.Collections.Generic;
using CompanyManager.BLL.Models;
using CompanyManager.BLL.Services.Interfaces;
using CompanyManager.DAL.Entities;
using CompanyManager.DAL.Repository;
using CompanyManager.BLL.Resources;
using CompanyManager.BLL.Exceptions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using AutoMapper;

namespace CompanyManager.BLL.Services
{
    public class RoleService : IRoleService
    {
        private readonly IRepository _repository;
        private readonly IMapper _mapper;
        public RoleService(IRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public List<RoleModel> GetAll()
        {
            var roles = _repository.GetList<Role>().ToList();
            var result = _mapper.Map<List<RoleModel>>(roles);
            return result;
        }

        public RoleModel GetRoleById(int roleId)
        {
            if (_repository.Any<Role>(c => c.Id == roleId))
            {
                var role = _repository.Get<Role>(c => c.Id == roleId);
                var result = _mapper.Map<RoleModel>(role);
                return result;
            }

            throw new NotFoundException(ExceptionResource.RoleNotFound);
        }

        public List<UserModel> GetUsersByRole(int roleId)
        {
            if (_repository.Any<Role>(c => c.Id == roleId))
            {
                var users = _repository.Get<Role>(x => x.Id == roleId, c => c.Include(z => z.Users)).Users.ToList();
                var result = _mapper.Map<List<UserModel>>(users);
                return result;
            }

            throw new NotFoundException(ExceptionResource.RoleNotFound);
        }

        public void CreateRole(RoleModel model)
        {
            if (_repository.Any<Role>(c => c.Name == model.Name))
                throw new ExistException(ExceptionResource.RoleNameExist);
            var role = _mapper.Map<Role>(model);
            _repository.Add(role);
            _repository.Commit();
        }

        public void UpdateRole(RoleModel model)
        {
            var role = _repository.Get<Role>(c => c.Id == model.Id);
            _mapper.Map(model, role);
            _repository.Update(role);
            _repository.Commit();
        }
    }
}
