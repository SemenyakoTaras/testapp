﻿using CompanyManager.BLL.Models;

namespace CompanyManager.BLL.Services.Interfaces
{
    public interface IAccountService
    {
        AccessModel Authorization(LoginModel model);
    }
}
