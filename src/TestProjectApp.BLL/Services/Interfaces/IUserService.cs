﻿using System.Collections.Generic;
using CompanyManager.BLL.Models;

namespace CompanyManager.BLL.Services.Interfaces
{
    public interface IUserService
    {
        List<UserModel> GetAll();
        UserModel GetUserById(int userId);
        void CreateUser(UserModel model);
        void UpdateUser(UserModel model);       
    }
}
