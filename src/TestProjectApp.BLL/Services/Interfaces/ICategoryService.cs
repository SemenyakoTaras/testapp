﻿using CompanyManager.BLL.Models;
using System.Collections.Generic;

namespace CompanyManager.BLL.Services.Interfaces
{
    public interface ICategoryService
    {
        CategoryModel Get(int Id);
        List<CategoryModel> GetAll();
        void CreateCategory(CategoryModel model);
        void RemoveCategory(int Id);
    }
}
