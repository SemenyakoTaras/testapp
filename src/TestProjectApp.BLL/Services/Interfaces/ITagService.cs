﻿using CompanyManager.BLL.Models;
using System.Collections.Generic;

namespace CompanyManager.BLL.Services.Interfaces
{
    public interface ITagService
    {
        void CreateTag(TagModel model);
        void RemoveTag(int Id);
        List<TagModel> GetAll();
        List<ArticleModel> GetArticlesByTags(string [] tags);
    }
}
