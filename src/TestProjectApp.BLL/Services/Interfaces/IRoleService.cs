﻿using System.Collections.Generic;
using CompanyManager.BLL.Models;

namespace CompanyManager.BLL.Services.Interfaces
{
    public interface IRoleService
    {
        List<RoleModel> GetAll();
        RoleModel GetRoleById(int roleId);
        List<UserModel> GetUsersByRole(int roleId);
        void CreateRole(RoleModel model);
        void UpdateRole(RoleModel model);

    }
}
