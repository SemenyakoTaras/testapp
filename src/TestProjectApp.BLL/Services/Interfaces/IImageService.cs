﻿using CompanyManager.BLL.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace CompanyManager.BLL.Services.Interfaces
{
    public interface IImageService
    {
        void AddImageToArticle(IFormFile file, int articleId);
        List<ImageModel> GetImagesForArticle(int articleId);
    }
}
