﻿using CompanyManager.BLL.Models;

namespace CompanyManager.BLL.Services.Interfaces
{
    public interface IArticleService
    {
        ArticleModel Get(string url, bool all = false);
        ArticleListModel GetAll(int page = 0, int pageSize=1, bool all = false);
        void Add(ArticleModel model);
        void Update(ArticleModel model);
       
    }
}
