﻿using CompanyManager.BLL.Services.Interfaces;
using System.Collections.Generic;
using CompanyManager.BLL.Models;
using Microsoft.AspNetCore.Http;
using CompanyManager.DAL.Repository;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using CompanyManager.DAL.Entities;
using CompanyManager.BLL.Exceptions;
using CompanyManager.BLL.Resources;
using System.Linq;

namespace CompanyManager.BLL.Services
{
    public class ImageService : IImageService
    {
        private readonly IRepository _repository;
        private readonly IHostingEnvironment _env;

        public ImageService(IRepository repository, IHostingEnvironment env)
        {
            _repository = repository;
            _env = env;
        }

        public void AddImageToArticle(IFormFile file, int articleId)
        {
            if (_repository.Any<Image>(c => c.Filename == file.FileName))
                throw new ExistException(ExceptionResource.ImageExist);

            if (!_repository.Any<Article>(c => c.Id == articleId))
                throw new ExistException(ExceptionResource.ArticleNotFound);

            try
            {
                var fstream = new FileStream(Path.Combine(_env.WebRootPath, "files"), FileMode.CreateNew);
                file.CopyTo(fstream);
            }
            catch (IOException ex)
            {
                throw new IOException("The file wasn't loaded.");
            }

            _repository.Add(new Image { Filename = file.FileName, ArticleId = articleId });
            _repository.Commit();
        }

        public List<ImageModel> GetImagesForArticle(int articleId)
        {
            if (!_repository.Any<Article>(c => c.Id == articleId))
                throw new ExistException(ExceptionResource.ArticleNotFound);

            var imgFilenames = _repository.Get<Article>(c => c.Id == articleId).Images.Select(z => z.Filename).ToList();

            var result = new List<ImageModel>();

            foreach (var name in imgFilenames)
                result.Add(new ImageModel { Filename = name, Bytes = File.ReadAllBytes(Path.Combine(_env.WebRootPath, "files", name)), Path = Path.Combine(_env.WebRootPath, "files", name) });

            return result;
        }
    }
}
