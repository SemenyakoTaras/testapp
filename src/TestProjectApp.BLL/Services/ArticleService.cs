﻿using CompanyManager.BLL.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using CompanyManager.BLL.Models;
using CompanyManager.DAL.Repository;
using AutoMapper;
using CompanyManager.DAL.Entities;
using CompanyManager.BLL.Exceptions;
using CompanyManager.BLL.Resources;
using Microsoft.EntityFrameworkCore;
using System;

namespace CompanyManager.BLL.Services
{
    public class ArticleService : IArticleService
    {
        private readonly IRepository _repository;
        private readonly IMapper _mapper;
        public ArticleService(IRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public void Add(ArticleModel model)
        {
            if (_repository.Any<Article>(c => c.URL == model.URL && c.Title == model.Title))
                throw new ExistException(ExceptionResource.ArticleExist);

            var article = _mapper.Map<Article>(model);

            _repository.Add(article);
            _repository.Commit();
        }

        public ArticleModel Get(string url, bool all = false)
        {
            if (!_repository.Any<Article>(c => c.URL == url))
                throw new NotFoundException(ExceptionResource.ArticleNotFound);
            
            var article = _repository.Get<Article>(c => c.URL == url && (all ? c.InDraft : !c.InDraft), z => z.Include(x => x.Category).Include(x => x.User));

            if (article == null) throw new NotFoundException(ExceptionResource.ArticleNotFound);

            var result = _mapper.Map<ArticleModel>(article);
            return result;
        }

        public ArticleListModel GetAll(int page = 0, int pageSize = 1, bool all = false)
        {
            var query = _repository.GetList<Article>();

            query = all ? query : query.Where(c => !c.InDraft);

            query = query
                .Include(z => z.User)
                .Include(c => c.Category)
                .OrderByDescending(c => c.DatePublished);

            var articles = query.Skip(page * pageSize).Take(pageSize).ToList();

            var result = _mapper.Map<List<ArticlePresentModel>>(articles);
            var total = Math.Ceiling((double)(query.Count() / pageSize));

            return new ArticleListModel() { Articles = result, Page = new PagerModel() { Current = page, Total = total } };
        }

        public void Update(ArticleModel model)
        {
            if (!_repository.Any<Article>(c => c.Id == model.Id))
                throw new ExistException(ExceptionResource.ArticleNotFound);

            var article = Mapper.Map<Article>(model);
            _repository.Update(article);
            _repository.Commit();
        }
    }
}
