﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using CompanyManager.BLL.Exceptions;
using CompanyManager.BLL.Models;
using CompanyManager.BLL.Resources;
using CompanyManager.BLL.Services.Interfaces;
using CompanyManager.DAL.Entities;
using CompanyManager.DAL.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using CompanyManager.DAL;

namespace CompanyManager.BLL.Services
{
    public class AccountService : IAccountService
    {
        private readonly IRepository _repository;

        public AccountService(IRepository repository)
        {
            _repository = repository;
        }

        public AccessModel Authorization(LoginModel model)
        {
            var passwordHash = HashMD5Extension.Hash(model.Password);
            var user = _repository.Get<User>(c => c.Email == model.Email && c.PasswordHash == passwordHash, x => x.Include(v => v.Role));
            var identity = GetIdentity(user);

            if (identity == null)
                throw new NotFoundException(ExceptionResource.UserNotFound);

            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: DateTime.Now,
                    claims: identity.Claims,
                    expires: DateTime.Now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var accessModel = new AccessModel()
            {
                Login = model.Email,
                AccessToken = encodedJwt,
                Role = user.Role.Name
            };

            return accessModel;
        }

        private ClaimsIdentity GetIdentity(User user)
        {
            if (user != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role.Name)
                };

                //ClaimsIdentity claimsIdentity = new ClaimsIdentity(new GenericIdentity(user.Email, "Token"), claims);
                ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }
            return null;
        }
    }
}
