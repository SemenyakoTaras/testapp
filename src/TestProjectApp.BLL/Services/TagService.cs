﻿using CompanyManager.BLL.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using CompanyManager.BLL.Models;
using CompanyManager.DAL.Repository;
using AutoMapper;
using CompanyManager.DAL.Entities;
using CompanyManager.BLL.Exceptions;
using CompanyManager.BLL.Resources;

namespace CompanyManager.BLL.Services
{
    public class TagService : ITagService
    {
        private readonly IRepository _repository;
        private readonly IMapper _mapper;
        public TagService(IRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public void CreateTag(TagModel model)
        {
            if (_repository.Any<Tag>(c => c.Name == model.Name))
                throw new ExistException(ExceptionResource.TagExist);

            var tag = _mapper.Map<Tag>(model);
            _repository.Add(tag);
            _repository.Commit();
        }

        public List<TagModel> GetAll()
        {
            var tags = _repository.GetList<Tag>();
            var result = _mapper.Map<List<TagModel>>(tags);
            return result;
        }

        public List<ArticleModel> GetArticlesByTags(string[] tags)
        {
            var articles = _repository.GetList<Article>(c => c.ArticleTags.Any(z => tags.Contains(z.Tag.Name))).OrderByDescending(c=>c.DatePublished).ToList();
            var result = _mapper.Map<List<ArticleModel>>(articles);
            return result;
        }

        public void RemoveTag(int Id)
        {
            if (_repository.Any<Tag>(c => c.Id == Id))
            {
                _repository.Remove<Tag>(c => c.Id == Id);
                _repository.Commit();
            }

            throw new NotFoundException(ExceptionResource.TagNotFound);
        }
    }
}
