﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace CompanyManager.BLL
{
    public class AuthOptions
    {
        public const string ISSUER = "http://localhost:4431/";
        public const string AUDIENCE = "http://localhost:4431/";
        const string KEY = "mysupersecret_secretkey!123";
        public const int LIFETIME = 60;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
