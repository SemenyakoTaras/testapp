﻿using CompanyManager.BLL.Services;
using CompanyManager.BLL.Services.Interfaces;
using CompanyManager.DAL;
using CompanyManager.DAL.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CompanyManager.WEB.Utility.MiddlewareComponents
{
    public static class RegisterService
    {
        public static void AddCustomServices(this IServiceCollection services, IConfigurationRoot Configuration)
        {
            services.AddEntityFramework(Configuration.GetConnectionString("DefaultConnection"));

            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            services.AddTransient<IRepository, Repository>();
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IRoleService, RoleService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<ITagService, TagService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IImageService, ImageService>();
            services.AddTransient<IArticleService, ArticleService>();
        }
    }
}
