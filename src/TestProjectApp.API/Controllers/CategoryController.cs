﻿using CompanyManager.BLL.Exceptions;
using CompanyManager.BLL.Models;
using CompanyManager.BLL.Resources;
using CompanyManager.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CompanyManager.WEB.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [Route("get")]
        [HttpGet]
        public ResultModel GetAll()
        {
            return new ResultModel(_categoryService.GetAll());
        }

        [Authorize(Roles = "Admin")]
        [Route("get/{categoryId}")]
        [HttpGet]
        public ResultModel Get(int categoryId)
        {
            return new ResultModel(_categoryService.Get(categoryId));
        }

        [Authorize(Roles = "Admin")]
        [Route("create")]
        [HttpPost]
        public ResultModel Create([FromBody] CategoryModel model)
        {
            if (!ModelState.IsValid)
                throw new ModelValidationException(ExceptionResource.InvalidModel);

            _categoryService.CreateCategory(model);
            return new ResultModel(new object());
        }

        [Authorize(Roles = "Admin")]
        [Route("delete/{categoryId}")]
        [HttpDelete]
        public ResultModel Delete(int categoryId)
        {            
            _categoryService.RemoveCategory(categoryId);
            return new ResultModel(new object());
        }
    }
}
