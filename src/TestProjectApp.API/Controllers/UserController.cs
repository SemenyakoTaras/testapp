﻿using CompanyManager.BLL.Exceptions;
using CompanyManager.BLL.Models;
using CompanyManager.BLL.Resources;
using CompanyManager.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CompanyManager.WEB.Controllers
{
    [ApiVersion("1.0")]
    [Authorize(Roles = "Admin")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        
        [Route("get")]
        [HttpGet]
        public ResultModel GetAll()
        {
            return new ResultModel(_userService.GetAll());
        }

        [Route("get/{userId}")]
        [HttpGet]
        public ResultModel GetUserById(int userId)
        {
            return new ResultModel(_userService.GetUserById(userId));
        }
                
        [Route("create")]
        [HttpPost]
        public ResultModel CreateUser([FromBody] UserModel model)
        {
            if (!ModelState.IsValid)
                throw new ModelValidationException(ExceptionResource.InvalidModel);

            _userService.CreateUser(model);
            return new ResultModel(new object());
        }
        
        [Route("update")]
        [HttpPut]
        public ResultModel UpdateUser([FromBody] UserModel model)
        {
            if (!ModelState.IsValid)
                throw new ModelValidationException(ExceptionResource.InvalidModel);

            _userService.UpdateUser(model);
            return new ResultModel(new object());
        }       
    }
}
