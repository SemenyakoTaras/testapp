﻿using System.Linq;
using CompanyManager.BLL.Exceptions;
using CompanyManager.BLL.Models;
using CompanyManager.BLL.Resources;
using CompanyManager.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace CompanyManager.WEB.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class ArticleController: Controller
    {
        private readonly IArticleService _articleService;
        public ArticleController(IArticleService articleService)
        {
            _articleService = articleService;
        }

        [Authorize(Roles = "Admin")]
        [Route("create")]
        [HttpPost]
        public ResultModel Create([FromBody] ArticleModel model)
        {
            if (!ModelState.IsValid)
                throw new ModelValidationException(ExceptionResource.InvalidModel);

            _articleService.Add(model);
            return new ResultModel(new object());
        }

        [AllowAnonymous]
        [Route("get/{page:int?}")]
        [HttpGet]
        public ResultModel GetAll(int page = 0)
        {
            var result = _articleService.GetAll(page);
            return new ResultModel(result);
        }

        //temporary solution
        [Authorize(Roles = "Admin")]
        [Route("get-admin/{page:int?}")]
        [HttpGet]
        public ResultModel GetAllForAdmin(int page = 0)
        {
            var result = _articleService.GetAll(page, all:true);
            return new ResultModel(result);
        }

        [AllowAnonymous]
        [Route("get/{url}")]
        [HttpGet]
        public ResultModel Get(string url)
        {
            var result = _articleService.Get(url);
            return new ResultModel(result);
        }

        //temporary solution
        [Authorize(Roles = "Admin")]
        [Route("get-admin/{url}")]
        [HttpGet]
        public ResultModel GetForAdmin(string url)
        {
            var result = _articleService.Get(url, true);
            return new ResultModel(result);
        }
    }
}
