﻿using CompanyManager.BLL.Exceptions;
using CompanyManager.BLL.Models;
using CompanyManager.BLL.Resources;
using CompanyManager.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CompanyManager.WEB.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class AccountController:Controller
    {
        private readonly IAccountService _accountService;
        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [AllowAnonymous]
        [Route("login")]
        [HttpPost]
        public ResultModel Login([FromBody] LoginModel model)
        {
            if (!ModelState.IsValid)
                throw new ModelValidationException(ExceptionResource.InvalidModel);

            var result = _accountService.Authorization(model);

            return new ResultModel(result);
        }
    }
}
