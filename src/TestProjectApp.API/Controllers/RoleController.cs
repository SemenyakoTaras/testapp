﻿using CompanyManager.BLL.Exceptions;
using CompanyManager.BLL.Models;
using CompanyManager.BLL.Resources;
using CompanyManager.BLL.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CompanyManager.WEB.Controllers
{
    [ApiVersion("1.0")]
    [Authorize(Roles = "Admin")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class RoleController : Controller
    {
        private readonly IRoleService _roleService;

        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }
        
        [Route("get")]
        [HttpGet]
        public ResultModel GetAll()
        {
            return new ResultModel(_roleService.GetAll());
        }

       
        [Route("get/{roleId}")]
        [HttpGet]
        public ResultModel GetRoleById(int roleId)
        {
            return new ResultModel(_roleService.GetRoleById(roleId));
        }

        
        [Route("get/{roleId}/users")]
        [HttpGet]
        public ResultModel GetUsersByRole(int roleId)
        {
            return new ResultModel(_roleService.GetUsersByRole(roleId));
        }

       
        [Route("create")]
        [HttpPost]
        public ResultModel CreateRole([FromBody] RoleModel model)
        {
            if (!ModelState.IsValid)
                throw new ModelValidationException(ExceptionResource.InvalidModel);

            _roleService.CreateRole(model);
            return new ResultModel(new object());
        }

        
        [Route("update")]
        [HttpPut]
        public ResultModel UpdateRole([FromBody] RoleModel model)
        {
            if (!ModelState.IsValid)
                throw new ModelValidationException(ExceptionResource.InvalidModel);
            _roleService.UpdateRole(model);
            return new ResultModel(new object());
        }
    }
}
