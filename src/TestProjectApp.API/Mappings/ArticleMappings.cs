﻿using AutoMapper;
using CompanyManager.BLL.Models;
using CompanyManager.DAL.Entities;

namespace CompanyManager.WEB.Mappings
{
    public class ArticleMappings : Profile
    {
        public ArticleMappings()
        {
            CreateMap<Article, ArticlePresentModel>()
                .ForMember(z => z.User, opt => opt.MapFrom(x => Mapper.Map<UserModel>(x.User)))
                .ForMember(z => z.Category, opt => opt.MapFrom(x => Mapper.Map<CategoryModel>(x.Category)));

            CreateMap<Article, ArticleModel>()
                .ForMember(z => z.Category, opt => opt.MapFrom(x => Mapper.Map<CategoryModel>(x.Category)))
                .ForMember(z => z.User, opt => opt.MapFrom(x => Mapper.Map<UserModel>(x.User)));

            CreateMap<ArticleModel, Article>()
                .ForMember(z => z.CategoryId, opt => opt.MapFrom(x => x.Category.Id))
                .ForMember(z => z.UserId, opt => opt.MapFrom(x => x.User.Id))
                .ForMember(z => z.User, opt => opt.MapFrom(x => Mapper.Map<UserModel>(x.User)))
                .ForMember(z => z.Category, opt => opt.MapFrom(x => Mapper.Map<CategoryModel>(x.Category)))
                .ForMember(z => z.Images, opt => opt.Ignore())
                .ForMember(z => z.ArticleTags, opt => opt.Ignore());
        }
    }
}
