﻿using AutoMapper;
using CompanyManager.BLL.Models;
using CompanyManager.DAL.Entities;

namespace CompanyManager.WEB.Mappings
{
    public class TagMappings: Profile
    {
        public TagMappings()
        {
            CreateMap<Tag, TagModel>();
            CreateMap<TagModel, Tag>();
        }
    }
}
