﻿using AutoMapper;
using CompanyManager.BLL.Models;
using CompanyManager.DAL.Entities;

namespace CompanyManager.WEB.Mappings
{
    public class CategoryMappings : Profile
    {
        public CategoryMappings()
        {
            CreateMap<Category, CategoryModel>();
            CreateMap<CategoryModel, Category>();
        }
    }
}
