﻿using AutoMapper;
using CompanyManager.BLL.Models;
using CompanyManager.DAL.Entities;

namespace CompanyManager.WEB.Mappings
{
    public class RoleMappings:Profile
    {
        public RoleMappings()
        {
            CreateMap<Role, RoleModel>();
            CreateMap<RoleModel,Role>();
        }
    }
}
