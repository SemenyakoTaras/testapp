﻿using AutoMapper;
using CompanyManager.BLL.Models;
using CompanyManager.DAL.Entities;

namespace CompanyManager.WEB.Mappings
{
    public class UserMappings : Profile
    {
        public UserMappings()
        {
            CreateMap<UserModel, User>();
            CreateMap<User, UserModel>();
        }
    }
}
