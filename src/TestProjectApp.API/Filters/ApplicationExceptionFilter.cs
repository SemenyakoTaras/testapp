﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using CompanyManager.BLL.Models;
using CompanyManager.BLL.Exceptions;
using Newtonsoft.Json;

namespace CompanyManager.WEB.Filters
{
    public class ApplicationExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var exceptionResult = ExceptionStrategyContext.Execute(context.Exception.GetType().Name, context);

            var errorModel = new ResultModel("") { Error = exceptionResult.Error };

            HttpResponse response = context.HttpContext.Response;
            response.StatusCode = (int)exceptionResult.StatusCode;
            response.ContentType = "application/json";

            var error = JsonConvert.SerializeObject(errorModel);
            response.WriteAsync(error);
        }
    }
}
